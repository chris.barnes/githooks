format:
	ruff check --fix && \
	ruff format

lint:
	ruff check
	ruff format --check

lint-types:
	mypy install.py hooks/
