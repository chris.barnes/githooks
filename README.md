# githooks

Collection of helpful git hooks, in python.

## Usage

```
usage: install.py [-h] [--only {commit-msg,pre-commit,pre-push,prepare-commit-msg}] [--uninstall] [--force] root

positional arguments:
  root                  Path to root of git repository

optional arguments:
  -h, --help            show this help message and exit
  --only {commit-msg,pre-commit,pre-push,prepare-commit-msg}, -o {commit-msg,pre-commit,pre-push,prepare-commit-msg}
                        Only this hook. Can be given multiple times
  --uninstall, -u       Uninstall hooks instead of installing them
  --force, -f           Overwrite (or uninstall) hooks from other sources
```

## Hooks

### `prepare-commit-msg`

Attempts to read a JIRA issue ID from the branch name;
if successful, prepends it to the commit message.

### `commit-msg`

Ensures that the commit message starts with a JIRA issue ID.

### `pre-commit`

Ensures that rust code is formatted.

### `pre-push` (Work In Progress)

Ensures that rust code is linted with clippy, and passes unit tests.

## Development

All scripts must be dependency-free.
Development dependencies are in [`requirements.txt`](./requirements.txt).

For maximum portability, all scripts should be compatible with the [oldest not-EOL python version](https://devguide.python.org/versions/).
At time of writing, this is python 3.8.
