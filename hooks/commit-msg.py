#!/usr/bin/env python3
"""Commit message hook to ensure the message starts with a JIRA issue ID."""

import logging
import re
import sys
from pathlib import Path

logger = logging.getLogger("git.hook.commit-msg")

issue_name_re = re.compile(r"^([A-Z]+-\d+)[\D\b].*")
no_issue_re = re.compile(r"^#no-issue", re.MULTILINE)


def main():
    fpath = Path(sys.argv[1])
    s = fpath.read_text().strip()
    if issue_name_re.search(s) is None:
        if no_issue_re.search(s):
            logger.info(
                "Commit message does not start with JIRA ID, but was auto-generated; allowing"
            )
            return 0
        no_comments = "\n".join(ln for ln in s.split("\n") if not ln.startswith("#"))
        print(no_comments, file=sys.stderr)
        logger.critical("Commit message above does not start with a JIRA ID; aborting")
        return 1
    return 0


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    sys.exit(main() or 0)
