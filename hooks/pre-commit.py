#!/usr/bin/env python3
"""Pre-commit hook which makes sure that rust code is formatted."""

from collections import defaultdict
import subprocess as sp
from pathlib import Path
import shlex
import logging
from typing import Any, List, DefaultDict, Set, Tuple, Optional
import sys

logger = logging.getLogger("git.hook.pre-commit")


def get_modified_files() -> List[Path]:
    args = shlex.split("git diff --cached --name-only --diff-filter=ACM")
    proc = sp.run(args, capture_output=True, check=True, text=True)
    out = []
    for ln in proc.stdout.splitlines():
        ln = ln.strip()
        if ln:
            out.append(Path(ln))
    return sorted(out)


def get_rust_files_by_crate(
    modified_files=None,
) -> Tuple[DefaultDict[Any, Set], List[Path]]:
    if modified_files is None:
        modified_files = get_modified_files()

    roots = defaultdict(set)
    loose = []

    for fpath in modified_files:
        if fpath.suffix != ".rs":
            continue

        for ancestor in fpath.parents:
            if ancestor in roots:
                break

            if ancestor.joinpath(".git").is_dir():
                loose.append(fpath)
                break

            if ancestor.joinpath("Cargo.toml").is_file():
                roots[ancestor].add(fpath.relative_to(ancestor))
                break
        else:
            loose.append(fpath)

    return roots, loose


def rust_format(fpaths: List[Path], root: Optional[Path]):
    path_strs = [str(p) for p in fpaths if p.suffix == ".rs"]
    if not path_strs:
        return

    if root is None:
        logger.info("Checking format of %s rust files without a crate", len(path_strs))
        prefix = shlex.split("rustfmt --check")
    else:
        logger.info("Checking format of %s rust files in %s", len(path_strs), root)
        prefix = shlex.split("cargo fmt --check --")

    fmt_args = prefix + path_strs

    cmd = sp.run(fmt_args, cwd=root, capture_output=True, text=True)
    if cmd.returncode == 0:
        return None

    if isinstance(cmd.args, str):
        args = cmd.args
    else:
        args = shlex.join(cmd.args)

    return (root, fpaths, args, cmd.stdout, cmd.stderr)


def main():
    crates, loose = get_rust_files_by_crate()

    output = []

    for root, fpaths in crates, loose.items():
        res = rust_format(fpaths, root)
        if res is not None:
            output.append(res)

    for root, _, args, stdout, stderr in output:
        logger.critical("Aborting commit, format command failed: `%s`", args)
        print("=== STDOUT ===")
        print(stdout)
        print("=== STDERR ===")
        print(stderr)

    if output:
        return 1


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    sys.exit(main() or 0)
