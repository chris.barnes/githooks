#!/usr/bin/env python3
"""Pre-push hook which makes sure tests and clippy are passing"""

import subprocess as sp
from pathlib import Path
import shlex
from typing import Optional, List, Tuple
from concurrent.futures import ThreadPoolExecutor
import os
import sys
import logging

logger = logging.getLogger("git.hook.pre-push")


def get_modified_files() -> List[Path]:
    args = shlex.split("git diff --cached --name-only --diff-filter=ACM")
    proc = sp.run(args, capture_output=True, check=True, text=True)
    out = []
    for ln in proc.stdout.splitlines():
        ln = ln.strip()
        if ln:
            out.append(Path(ln))
    return sorted(out)


def get_crate_roots(modified=None) -> List[Path]:
    if not modified:
        modified = get_modified_files()

    modified_rs = [m for m in modified if m.suffix == ".rs"]

    roots = set()

    for fpath in modified_rs:
        for ancestor in fpath.parents:
            if ancestor in roots:
                continue

            if ancestor.joinpath("Cargo.toml").is_file():
                roots.add(ancestor)
                continue

    return sorted(roots)


def get_workspace_roots(crate_roots=None):
    if crate_roots is None:
        crate_roots = get_crate_roots()

    roots = set()

    for dpath in crate_roots:
        last = dpath
        for ancestor in dpath:
            if ancestor in roots:
                continue

            if ancestor.joinpath("Cargo.toml").is_file():
                last = ancestor
        roots.append(last)

    return sorted(roots)


def clippy_root(dpath: Path) -> Optional[Tuple[Path, str]]:
    logger.info("Running clippy in %s", dpath)
    args = shlex.split("cargo clippy --all-targets --all-features -- -D warnings")
    proc = sp.run(args, cwd=dpath, check=False, text=True, capture_output=True)
    if proc.returncode != 0:
        return dpath, proc.stderr
    else:
        return None


def pretest_root(dpath: Path):
    logger.info("Pre-building tests from %s", dpath)
    args = shlex.split("cargo test --all-features thisisnotatestname")
    sp.run(args, cwd=dpath, check=True, text=True, capture_output=True)


def test_root(dpath: Path) -> Optional[Tuple[Path, str]]:
    logger.info("Testing %s", dpath)
    args = shlex.split("cargo test --all-features --workspace")
    proc = sp.run(args, cwd=dpath, check=False, text=True, capture_output=True)
    if proc.returncode != 0:
        return dpath, proc.stdout
    else:
        return None


def underline(s: str, c="-"):
    return s + "\n" + c * len(s)


def main():
    modified_crates = get_crate_roots()
    modified_workspaces = get_workspace_roots(modified_crates)
    for c in modified_workspaces:
        pretest_root(c)

    with ThreadPoolExecutor(os.cpu_count()) as p:
        clippy_results = p.map(clippy_root, modified_crates)
        test_results = p.map(test_root, modified_workspaces)

        failed_clippy = [r for r in clippy_results if r is not None]
        failed_tests = [r for r in test_results if r is not None]

    out = 0
    if failed_clippy:
        out += 1
        print(underline("Failed clippy lints", "=") + "\n", file=sys.stderr)
        for fpath, stderr in failed_clippy:
            print(underline(str(fpath)) + "\n", file=sys.stderr)
            print(stderr + "\n", file=sys.stderr)

    if failed_tests:
        out += 2
        print(underline("Failed tests", "=") + "\n", file=sys.stderr)
        for fpath, stderr in failed_tests:
            print(underline(str(fpath)) + "\n", file=sys.stderr)
            print(stderr + "\n", file=sys.stderr)

    sys.exit(out)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    sys.exit(main() or 0)
