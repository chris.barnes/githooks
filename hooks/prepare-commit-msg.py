#!/usr/bin/env python3
"""Commit message preparation hook which populates the commit message
with a JIRA issue ID, if the branch name looks like one and the commit message is empty."""

import subprocess as sp
import shlex
import logging
import re
import sys
from pathlib import Path

logger = logging.getLogger("git.hook.prepare-commit-msg")

issue_name_re = re.compile(r"^([a-z]+-\d+).*", re.IGNORECASE)


def get_branch_name() -> str:
    args = shlex.split("git rev-parse --abbrev-ref HEAD")
    proc = sp.run(args, capture_output=True, check=True, text=True)
    return proc.stdout.strip()


def is_empty(s):
    for ln in s.split("\n"):
        stripped = ln.strip()
        if stripped and not stripped.startswith("#"):
            return False

    return True


def main():
    branch_name = get_branch_name()
    match = issue_name_re.fullmatch(branch_name)
    if match is None:
        return None

    fpath = Path(sys.argv[1])
    existing = fpath.read_text()
    if is_empty(existing):
        prefix = match.groups()[0].upper() + ": \n"
        fpath.write_text(prefix + existing)
    else:
        suffix = "\n#no-issue"
        fpath.write_text(existing + suffix)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    sys.exit(main() or 0)
