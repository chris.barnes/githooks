#!/usr/bin/env python3
"""Install hooks in the given directory"""

from argparse import ArgumentParser
from pathlib import Path
import logging
from typing import Dict, Tuple, Set
import sys

logger = logging.getLogger("install-hooks")

here = Path(__file__).resolve().parent
hook_dir = here / "hooks"


names = {
    "pre-commit",
    "prepare-commit-msg",
    "commit-msg",
    "post-commit",
    "pre-rebase",
    "post-rewrite",
    "post-checkout",
    "post-merge",
    "pre-push",
    "pre-auto-gc",
}


def present_hooks() -> Tuple[Dict[Path, str], Set[str]]:
    hook_paths = dict()
    used_names = set()

    for hook_path in hook_dir.iterdir():
        name = hook_path.name.split(".", 1)[0]

        if name in names:
            hook_paths[hook_path.resolve()] = name
            used_names.add(name)

    return hook_paths, used_names


def main():
    hook_paths, used_names = present_hooks()

    parser = ArgumentParser()
    parser.add_argument("root", help="path to root of git repository", type=Path)
    parser.add_argument(
        "--only",
        "-o",
        help="only this hook; can be given multiple times",
        action="append",
        choices=sorted(used_names),
    )
    parser.add_argument(
        "--uninstall",
        "-u",
        help="uninstall hooks instead of installing them",
        action="store_true",
    )
    parser.add_argument(
        "--force",
        "-f",
        help="overwrite (or uninstall) hooks from other sources",
        action="store_true",
    )

    parsed = parser.parse_args()

    git_hook_root = parsed.root.joinpath(".git/hooks")
    if not git_hook_root.is_dir():
        logger.critical("Given path is not a git root: %s", git_hook_root)
        return 1

    if parsed.only:
        name_set = used_names.intersection(parsed.only)
    else:
        name_set = used_names

    excluded = set()

    for name in sorted(name_set):
        git_hook_path = git_hook_root / name
        if not git_hook_path.exists():
            continue

        if git_hook_path.resolve() in hook_paths:
            git_hook_path.unlink()
            logger.info("Removed existing symlink at %s", git_hook_path)
        else:
            if parsed.force:
                logger.warn("Removing hook from another source at %s", git_hook_path)
                git_hook_path.unlink()
                logger.info("Removed existing symlink at %s", git_hook_path)
            else:
                excluded.add(name)

    if parsed.uninstall:
        return 0

    for hook_path, name in hook_paths.items():
        if name in excluded or name not in name_set:
            continue
        git_hook_path = git_hook_root / name
        git_hook_path.symlink_to(hook_path)
        logger.info("Wrote link %s -> %s", git_hook_path, hook_path)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    sys.exit(main() or 0)
